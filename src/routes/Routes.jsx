import React, { Fragment } from 'react'
import { Route, Switch, BrowserRouter } from 'react-router-dom';

import Home from "./home/HomePage"
import Header from "../components/common/header/Header"
import NotFound from '../routes/notfound/NotFoundPage';
import Footer from '../components/common/footer/Footer'

export default function Routes(props) {
    return (
        <Fragment>
            <BrowserRouter>
                <Header />
                <main>
                    <Switch>
                        <Route exact path="/" component={Home} />
                        <Route exact path="/home" component={Home} />
                        <Route component={NotFound} />
                    </Switch>
                </main>
                <Footer />                
            </BrowserRouter>
        </Fragment>
    )
}