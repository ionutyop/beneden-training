import React from "react";
import Slider from '../../components/slider/Slider'
import Card from '../../components/card/Card'
import InfoBox from "../../components/info-box/InfoBox";

const images = ["https://wallpaperbro.com/img/97484.jpg","https://media.boingboing.net/wp-content/uploads/2017/03/surprised-cat-04.jpg" , "https://data.1freewallpapers.com/download/fluffy-cat.jpg"];

export default function Home(props) {
    return (
        <div className="site-width">
            <Slider images={images}/>
            <div className="card-list">
                <Card icon={"icon-twitch"}/>
                <Card icon={"icon-steam"}/>
                <Card icon={"icon-android"}/>
                <Card icon={"icon-github"}/>
            </div>
            <InfoBox />
        </div>
    );
}