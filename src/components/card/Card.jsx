import React from 'react'

import './Card.scss'
import Brain from '../../assets/brain-big.jpg'


export default function Card(props) {

    return (

        <div className="card">
            <div className="card-logo">
                <span className={props.icon}></span>
            </div>

            <div className="card-content">
                <img src={Brain} alt="text" className="card-content-image" />
                <p className="card-content-text">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla id laoreet sapien, quis porta leo. Mauris feugiat blandit nibh, eget consequat orci sodales ac.
                </p>
            </div>
        </div>
    );

}