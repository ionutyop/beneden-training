import React from 'react'

import './Slider.scss'

class Slider extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            index: 0 ,
            images: props.images,
        };
        this.incrementIndex= this.incrementIndex.bind(this);
        this.decrementIndex= this.decrementIndex.bind(this);

    }

    incrementIndex(){
        this.setState((state) => {
            return {index: Math.min(state.index+1,state.images.length-1)}
          });
    }
    decrementIndex(){
        this.setState((state) => {
            return {index: Math.max(state.index-1,0)}
          });
    }

    render() {
        return (
            <div className="carousel">
                <img className="carousel-image" srcSet={this.state.images[this.state.index]} alt="Nature" />

                <div className="carousel-controls">
                    <button onClick={this.decrementIndex}>&#10094;</button>
                    <button onClick={this.incrementIndex}>&#10095;</button>
                </div>
            </div>
        );
    }
}


export default Slider;