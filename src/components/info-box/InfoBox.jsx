import React from 'react';
import Button from '@material-ui/core/Button'
import './InfoBox.scss';

export default function InfoBox(props) {
    return (
        <div className="info-box">
            <h1 className="info-box-title">Lorem ipsum dolor sit amet</h1>
            <h4 className="info-box-subtitle">consectetur adipiscing elit</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla id laoreet sapien, quis porta leo.Mauris feugiat blandit nibh, eget consequat orci sodales ac. Maecenas finibus, arcu sit amet
                posuere dapibus, risus mi vulputate erat, nec ultrices ex nisl at lectus.
                <span style={{ background: "#d1f8d2" }}> Quisque rutrum laoreet mollis. </span> </p>
            <Button variant="contained" color="primary">Push the button</Button>
        </div>
    );
}