import React from 'react'
import { NavLink } from 'react-router-dom'

import './Header.scss'
import logo from '../../../assets/logo.jpg'

export default function Header(props) {
    const activeStyle = { color: "green" };
    return (
        <header>
            <nav className="navbar site-width">
                <div className="navbar-logo">
                    <img src={logo} alt="Ness" />
                </div>
                <ul className='navbar-links'>
                    <NavLink activeStyle={activeStyle} to='/' exact>Curs1</NavLink>
                    <NavLink activeStyle={activeStyle} to='/curs1' exact>Curs2</NavLink>
                    <NavLink activeStyle={activeStyle} to='/curs2' exact>Curs3</NavLink>
                </ul>
            </nav>
        </header>
    )
}