import React from 'react'
import "./Footer.scss"


export default function Footer(props) {

    return (
        <footer className="footer">
            &copy; 2019 Benenden frontend learning
        </footer>
    );

}